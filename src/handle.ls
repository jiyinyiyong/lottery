
query = -> document.querySelector it
log = -> console.log.apply console, arguments

run = no

window.onload = ->
  canvas = query \canvas
  lottery = query \#lottery
  go = query \#go
  # log lottery

  window.ctx = canvas.get-context \2d

  cx = 400
  cy = 200
  r = 180
  point = 0
  p = [0.2 0.24 0.54 0.6 0.7 0.8 0.9 1]
  choice = [
    {delta: p.0, color: "hsl(0,40%,60%)"}
    {delta: p.1, color: "hsl(120,40%,90%)"}
    {delta: p.2, color: "hsl(240,40%,60%)"}
    {delta: p.3, color: "hsl(240,90%,80%)"}
    {delta: p.4, color: "hsl(140,90%,90%)"}
    {delta: p.5, color: "hsl(240,90%,80%)"}
    {delta: p.6, color: "hsl(240,90%,70%)"}
    {delta: p.7, color: "hsl(40,90%,50%)"}
  ]

  cal = -> Math.PI * 2 * it
  ctx
    ..stroke-style = "hsl(0,0%,50%)"

  set-font = (count) ->
    number = 1 - count
    # log number
    get = ""
    if number < p.0
      get = "1500 魔力值"
    else if number < p.1
      get = "无限量拷片"
    else if number < p.2
      get = "1000 魔力值"
    else if number < p.3
      get = "“黄瓜”一根"
    else if number < p.4
      get = "实物 UT 勋章"
    else if number < p.5
      get = "15G 上传量"
    else if number < p.6
      get = "2000 魔力值"
    else if number < p.7
      get = "10G 上传量"
    else
      get = "无限量拷片"
    lottery.inner-text = get

  draw = (count) ->
    theta = cal count
    ctx
      ..clear-rect 0 0 600 600
      ..translate cx, cy
      ..rotate (theta + (cal 0.5))
      ..translate -cx, -cy
    last-delta = 0
    choice.forEach (now) ->
      {delta, color} = now
      ctx
        ..begin-path!
        ..move-to cx, cy
        ..fill-style = color
        ..arc cx, cy, r, (cal last-delta), (cal delta), no
        ..line-to cx, cy
        ..close-path!
        ..fill!
      last-delta := delta
    ctx
      ..translate cx, cy
      ..rotate -(theta + (cal 0.5))
      ..translate -cx, -cy
      ..begin-path!
      ..move-to (cx - r/2), cy
      ..line-to (cx - r*3/2), cy
      ..stroke!
    set-font count

  theta = 0

  draw 0
  do window.refresh = ->
    if run
      theta := theta - 0.091
      if theta <0 then theta := theta + 1
      draw theta
    set-timeout refresh, 40

  query \body .onkeydown = ->
    if it.key-code is 32
      toggle!

  window.style = document.body.style

  go.onclick = ->
    log "click"
    toggle!

  window.toggle = ->
    if go.inner-text is "Start"
      go.inner-text = "Go"
      run := yes
    else
      go.inner-text = "Start"
      run := no

# window.onfocus = ->
#   if (query \#go).inner-text is "Start"
#     style.webkit-filter = ""
#     style.background = "hsl(0,80%,96%)"
#     run := yes

# window.onblur = ->
#   if (query \#go).inner-text is "Go"
#     style.webkit-filter = "grayscale(0.5) blur(4px)"
#     style.background = "hsl(0,80%,95%)"
#     toggle!