var query, log, run;
query = function(it){
  return document.querySelector(it);
};
log = function(){
  return console.log.apply(console, arguments);
};
run = false;
window.onload = function(){
  var canvas, lottery, go, cx, cy, r, point, p, choice, cal, x$, setFont, draw, theta;
  canvas = query('canvas');
  lottery = query('#lottery');
  go = query('#go');
  window.ctx = canvas.getContext('2d');
  cx = 400;
  cy = 200;
  r = 180;
  point = 0;
  p = [0.2, 0.24, 0.54, 0.6, 0.7, 0.8, 0.9, 1];
  choice = [
    {
      delta: p[0],
      color: "hsl(0,40%,60%)"
    }, {
      delta: p[1],
      color: "hsl(120,40%,90%)"
    }, {
      delta: p[2],
      color: "hsl(240,40%,60%)"
    }, {
      delta: p[3],
      color: "hsl(240,90%,80%)"
    }, {
      delta: p[4],
      color: "hsl(140,90%,90%)"
    }, {
      delta: p[5],
      color: "hsl(240,90%,80%)"
    }, {
      delta: p[6],
      color: "hsl(240,90%,70%)"
    }, {
      delta: p[7],
      color: "hsl(40,90%,50%)"
    }
  ];
  cal = function(it){
    return Math.PI * 2 * it;
  };
  x$ = ctx;
  x$.strokeStyle = "hsl(0,0%,50%)";
  setFont = function(count){
    var number, get;
    number = 1 - count;
    get = "";
    if (number < p[0]) {
      get = "1500 魔力值";
    } else if (number < p[1]) {
      get = "无限量拷片";
    } else if (number < p[2]) {
      get = "1000 魔力值";
    } else if (number < p[3]) {
      get = "“黄瓜”一根";
    } else if (number < p[4]) {
      get = "实物 UT 勋章";
    } else if (number < p[5]) {
      get = "15G 上传量";
    } else if (number < p[6]) {
      get = "2000 魔力值";
    } else if (number < p[7]) {
      get = "10G 上传量";
    } else {
      get = "无限量拷片";
    }
    return lottery.innerText = get;
  };
  draw = function(count){
    var theta, x$, lastDelta;
    theta = cal(count);
    x$ = ctx;
    x$.clearRect(0, 0, 600, 600);
    x$.translate(cx, cy);
    x$.rotate(theta + cal(0.5));
    x$.translate(-cx, -cy);
    lastDelta = 0;
    choice.forEach(function(now){
      var delta, color, x$;
      delta = now.delta, color = now.color;
      x$ = ctx;
      x$.beginPath();
      x$.moveTo(cx, cy);
      x$.fillStyle = color;
      x$.arc(cx, cy, r, cal(lastDelta), cal(delta), false);
      x$.lineTo(cx, cy);
      x$.closePath();
      x$.fill();
      return lastDelta = delta;
    });
    x$ = ctx;
    x$.translate(cx, cy);
    x$.rotate(-(theta + cal(0.5)));
    x$.translate(-cx, -cy);
    x$.beginPath();
    x$.moveTo(cx - r / 2, cy);
    x$.lineTo(cx - r * 3 / 2, cy);
    x$.stroke();
    return setFont(count);
  };
  theta = 0;
  draw(0);
  (window.refresh = function(){
    if (run) {
      theta = theta - 0.091;
      if (theta < 0) {
        theta = theta + 1;
      }
      draw(theta);
    }
    return setTimeout(refresh, 40);
  })();
  query('body').onkeydown = function(it){
    if (it.keyCode === 32) {
      return toggle();
    }
  };
  window.style = document.body.style;
  go.onclick = function(){
    log("click");
    return toggle();
  };
  return window.toggle = function(){
    if (go.innerText === "Start") {
      go.innerText = "Go";
      return run = true;
    } else {
      go.innerText = "Start";
      return run = false;
    }
  };
};